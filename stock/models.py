from django.db import models
from ckeditor.fields import RichTextField

class Product(models.Model):
	nome = models.CharField(max_length=45)
	descricao = RichTextField()
	codigo = models.CharField(max_length=45)
	imagem = models.ImageField(null=True, upload_to='produtos')
	quantidade = models.PositiveIntegerField()

	def __str__(self):
		return self.codigo + ' - ' + self.nome